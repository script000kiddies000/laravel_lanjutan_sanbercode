<?php

class Hewan{
	public $nama;
	public $darah = 50;
	public $jumlahKaki;
	public $keahlian;	

	public function get_name(){
		return $this->nama;
	}

	public function get_darah(){
		return $this->darah;
	}
	public function get_attackPower(){
		return $this->attackPower;
	}

	public function get_defencePower(){
		return $this->defencePower;
	}

	public function atraksi(){
		return $this->nama ." sedang ". $this->keahlian;
	}
}

class Fight extends Hewan{
	public $attackPower;
	public $defencePower;

	public function serang(){
		return $this->nama." sedang menyerang ";
	}

	public function diserang(){
		return $this->nama." sedang diserang";
	}


}

class Elang extends Fight{
	public function __construct(){
		$this->nama = 'Elang_1';
		$this->jumlahKaki = 2;
		$this->keahlian = 'terbang tinggi';

		$this->attackPower = 10;
		$this->defencePower = 5;
	}

	public function getInfoHewan(){
		return "Nama: ".$this->nama."<br>jenis hewan: Elang "."<br>Jumlah Kaki: ".$this->jumlahKaki."<br>Keahlian: ".$this->keahlian."<br>Darah: ".$this->darah."<br>Attack Power: ".$this->attackPower."<br>Defense Power: ".$this->defencePower;
	}

}



class Harimau extends Fight{
	public function __construct(){
		$this->nama = 'Harrimau_1';
		$this->jumlahKaki = 4;
		$this->keahlian = 'lari cepat';

		$this->attackPower = 7;
		$this->defencePower = 8;
	}

	public function getInfoHewan(){
		return "Nama: ".$this->nama."<br>jenis hewan: Harimau "."<br>Jumlah Kaki: ".$this->jumlahKaki."<br>Keahlian: ".$this->keahlian."<br>Darah: ".$this->darah."<br>Attack Power: ".$this->attackPower."<br>Defense Power: ".$this->defencePower;
	}
	
}


$harimau = new Harimau();

$elang = new Elang();
echo $elang->getInfoHewan()."<br><br>";
echo $elang->atraksi().'<br>';
echo $elang->serang().$harimau->get_name().'<br>';
echo $elang->diserang().'<br>';
$darah = $elang->get_darah() - ($harimau->get_attackPower() / $elang->get_defencePower());
echo "Darah sekarang ".$darah."<br>";


?>

