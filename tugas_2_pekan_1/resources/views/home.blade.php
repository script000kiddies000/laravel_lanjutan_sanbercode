@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
                <div class="row">
                <div class="col-md-4">
                    <a href="/guest"><button class="btn btn-primary">Halaman Tamu</button></a>
                 </div>
                 @if (auth()->user()->role_id==0)
                 <div class="col-md-4">
                    <a href="/admin"><button class="btn btn-danger" disabled>Halaman Admin</button></a>
                </div>
                 @else
                 <div class="col-md-4">
                    <a href="/admin"><button class="btn btn-primary">Halaman Admin</button></a>
                </div>
                 @endif
                 @if (auth()->user()->role_id!=2)
                 <div class="col-md-4">
                    <a href="/superadmin"><button class="btn btn-danger" disabled>Halaman Super Admin</button></a>
                </div>
                 @else
                 <div class="col-md-4">
                    <a href="/superadmin"><button class="btn btn-primary">Halaman Super Admin</button></a>
                </div>
                 @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
